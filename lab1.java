import java.io.File;
import java.util.Collections;

public class lab1 {

    public static void main(String[] args) {
        String currentDir = System.getProperty("user.dir");
        System.out.println(currentDir);
        printDirectoryTree(new File(currentDir), 0);
    }

    private static void printDirectoryTree(File folder, int level) {
        String indent = String.join("", Collections.nCopies(level, "\t"));
        System.out.println(indent + folder.getName());
        for (File file : folder.listFiles()) {
            if (file.isDirectory()) {
                printDirectoryTree(file, level + 1);
            }
            else {
                System.out.println(indent + "\t" + file.getName());
            }
        }
    }
}
